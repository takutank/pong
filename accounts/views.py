from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')

    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})

#formのエラーメッセージを表示する方法
#https://naruport.com/blog/2019/7/27/django-show-errors-of-form/
#淺野・井出先生の協力有り
