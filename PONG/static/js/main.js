// phina.js をグローバル領域に展開
phina.globalize();

// Matching.htmlからのデータ共有
var raw_data_main = document.getElementById("data_json");
var data_json = JSON.parse(raw_data_main.textContent);
/* ---socketのやつ使い方ゾーン---
// 宣言
var Socket = data_json["socket"]

// -----以下が使い方-----

// もし切断した時用
Socket.onclose = function(e) {
  console.error('Websocket closed unexpectedly');
};

// 送られてきたメッセージを取得する
Socket.onmessage = function(e){
  // JSONデータを取得（JSON.parse()で復元出来る）
  var raw = JSON.parse(e.data)["message"];
  var mes = raw["message"];

  // typeを取得してどういった挙動にしたいかを決定する
  switch (raw["type"]) {
    case //そのデータの名前(string):
      break;
    default:
      break;
    }
}
// メッセージを全員に送信したい
Socket.send(JSON.stringify({
  "type":(stringで指定),
  "message":(ペイロード)
}));

// 概要
typeには送信するデータが何なのかを指定（受け取りの時はデータの判別に使う）
messageには何を入れても良い。 Jsonになってるので、構造化すると読み出しやすくなる

現在対応してるtypeの名前：
- "sync_user"
- "status_update"
  - messageには "status"フィールドが必要。
  - statusフィールドには
    - "gameover"
    - の他、何でも指定可能。

*/

//変数定義
var SCREEN_WIDTH   = 960;
//var SCREEN_WIDTH   = 640;
var SCREEN_HEIGHT  = 960;
var MAX_PER_LINE   = 8;
var BLOCK_NUM      = MAX_PER_LINE * 5;
var BLOCK_SIZE     = 64;
var BORAD_PADDING  = 50;
var PADDLE_WIDTH   = 150;
var PADDLE_HEIGHT  = 32;
var BALL_RADIUS    = 16;
var BALL_SPEED     = 16;

var BOARD_SIZE     = SCREEN_WIDTH - BORAD_PADDING * 2;
//var BOARD_OFFSET_X = 150;
var BOARD_OFFSET_X = 20 + BORAD_PADDING + BLOCK_SIZE / 2;
var BOARD_OFFSET_Y = 150 + BORAD_PADDING + BLOCK_NUM / 2;

var count1 = 0;
var count2 = 0;
var count3 = 0;
var count4 = 0;

// MainScene クラスを定義
phina.define('MainScene', {
  superClass: 'DisplayScene',
  init: function(options) {
    this.superInit(options);
    // 背景色を指定
    //this.backgroundColor = '#444';
    // スコアラベルを生成
    this.scoreLabel = Label('0').addChildTo(this);
    this.scoreLabel.x = this.gridX.center(); // x 座標
    this.scoreLabel.y = this.gridY.span(1); // y 座標
    this.scoreLabel.fill = 'white'; // 塗りつぶし色

    //グループ
    this.group = DisplayElement().addChildTo(this);

    var gridX = Grid(BOARD_SIZE, MAX_PER_LINE);
    var gridY = Grid(BOARD_SIZE, MAX_PER_LINE);

    //var self = this;

    (BLOCK_NUM).times(function(i){
      //グリッド上でのインデックス
      var xIndex = Math.floor(i % MAX_PER_LINE);
      var yIndex = Math.floor(i / MAX_PER_LINE);
      var angle = (360) / BLOCK_NUM * i;
      var block = Block(angle).addChildTo(this.group).setPosition(100, 100);

      block.x = gridX.span(xIndex) + BOARD_OFFSET_X;
      block.y = gridY.span(yIndex)+BOARD_OFFSET_Y;
    }, this);

    //ボール
    this.ball = Ball().addChildTo(this);

    //パドル1
    this.paddle1 = Paddle().addChildTo(this);
    this.paddle1.setPosition(this.gridX.center(), this.gridY.span(15));
    this.paddle1.hold(this.ball);

    //パドル2
    this.paddle2 = Paddle().addChildTo(this);
    this.paddle2.setPosition(this.gridX.center(), this.gridY.span(1));
    //this.paddle2.hold(this.ball);

    //パドル3
    this.paddle3 = Paddle_h().addChildTo(this);
    this.paddle3.setPosition(this.gridX.span(15), this.gridY.center());
    //this.paddle2.hold(this.ball);

    //パドル4
    this.paddle4 = Paddle_h().addChildTo(this);
    this.paddle4.setPosition(this.gridX.span(1), this.gridY.center());
    //this.paddle2.hold(this.ball);

    // タッチでゲーム開始
    this.ballSpeed = 0;
    this.one('pointend', function() {
      this.paddle1.release();
      this.ballSpeed = BALL_SPEED;
    });

    // スコア
    this.score = 0;
    // 時間
    this.time = 0;
    // コンボ
    this.combo = 0;
  },

  update: function(app) {
    // タイムを加算
    this.time += app.deltaTime;

    if (data_json["player_role"] == "Player0"){
      // パドル1移動（下）ユーザー同期（位置の同期）
      this.paddle1.x = app.pointer.x;
      if (this.paddle1.left < 0) {
        this.paddle1.left = 0;
      }
      if (this.paddle1.right > this.gridX.width) {
        this.paddle1.right = this.gridX.width;
      }
    }

    if (data_json["player_role"] == "Player1"){
      // パドル2移動（上）ユーザー同期（位置の同期）
      this.paddle2.x = app.pointer.x;
      if (this.paddle2.left < 0) {
        this.paddle2.left = 0;
      }
      if (this.paddle2.right > this.gridX.width) {
        this.paddle2.right = this.gridX.width;
      }
    }

    if (data_json["player_role"] == "Player2"){
      // パドル3移動（右）ユーザー同期（位置の同期）
      this.paddle3.y = app.pointer.y;
      if (this.paddle3.top < 0) {
        this.paddle3.top = 0;
      }
      if (this.paddle3.bottom > this.gridY.width) {
        this.paddle3.bottom = this.gridY.width;
      }
    }

    if (data_json["player_role"] == "Player3"){
      // パドル4移動（左）ユーザー同期（位置の同期）
      this.paddle4.y = app.pointer.y;
      if (this.paddle4.top < 0) {
        this.paddle4.top = 0;
      }
      if (this.paddle4.bottom > this.gridY.width) {
        this.paddle4.bottom = this.gridY.width;
      }
    }

    // スピードの数分, 移動と衝突判定を繰り返す
    (this.ballSpeed).times(function() {
      this.ball.move();
      this.checkHit();
    }, this);

    // ブロックがすべてなくなったらクリア ゲームオーバー同期、ゲームクリア同期
    //if (this.group.children.length <= 0) {
    if (count1 == 7 | count2 == 7 | count3 == 7 | count4 == 7) {
      this.gameover();
    } else if (this.group.children.length <= 0) {
      this.gameclear();
    }
  },

  checkHit: function() {
    //
    var ball = this.ball;

    // 画面外対応 点数同期
    if (ball.left < 0) {
      ball.left = 0;
      ball.reflectX();
      count4 += 1
    }
    if (ball.right > this.gridX.width) {
      ball.right = this.gridX.width;
      ball.reflectX();
      count3 += 1;
    }
    if (ball.top < 0) {
      ball.top = 0;
      ball.reflectY();
      count2 += 1;
    }
    if (ball.bottom > this.gridY.width) {
      ball.bottom = this.gridY.width;
      ball.reflectY();
      //this.gameover();
      count1 += 1;
    }

    // ボールとパドル1
    if (ball.hitTestElement(this.paddle1)) {
      ball.bottom = this.paddle1.top;

      var dx = ball.x - this.paddle1.x;
      ball.direction.x = dx;
      ball.direction.y = -80;
      ball.direction.normalize();

      // speed up
      this.ballSpeed += 1;

      // コンボ数をリセット
      this.combo = 0;
    }

    // ボールとパドル2
    if (ball.hitTestElement(this.paddle2)) {
      ball.top = this.paddle2.bottom;

      var dx = ball.x - this.paddle2.x;
      ball.direction.x = -dx;
      ball.direction.y = 80;
      ball.direction.normalize();

      // speed up
      this.ballSpeed += 1;

      // コンボ数をリセット
      this.combo = 0;
    }

    // ボールとパドル3
    if (ball.hitTestElement(this.paddle3)) {
      ball.right = this.paddle3.left;

      var dy = ball.y - this.paddle3.y;
      ball.direction.y = dy;
      ball.direction.x = -80;
      ball.direction.normalize();

      // speed up
      this.ballSpeed += 1;

      // コンボ数をリセット
      this.combo = 0;
    }

      // ボールとパドル4
    if (ball.hitTestElement(this.paddle4)) {
      ball.left = this.paddle4.right;

      var dy = ball.y - this.paddle4.y;
      ball.direction.y = -dy;
      ball.direction.x = 80;
      ball.direction.normalize();

      // speed up
      this.ballSpeed += 1;

      // コンボ数をリセット
      this.combo = 0;
    }

    this.group.children.some(function(block) {
      // ヒット
      if (ball.hitTestElement(block)) {
        var dq = Vector2.sub(ball, block);

        if (Math.abs(dq.x) < Math.abs(dq.y)) {
          ball.reflectY();
          if (dq.y >= 0) {
            ball.top = block.bottom;
          }
          else {
            ball.bottom = block.top;
          }
        }
        else {
          ball.reflectX();
          if (dq.x >= 0) {
            ball.left = block.right;
          }
          else {
            ball.right = block.left;
          }
        }

        block.remove();

        this.combo += 1;
        this.score += this.combo*100;

        var c = ComboLabel(this.combo).addChildTo(this);
        c.x = this.gridX.span(12) + Math.randint(-50, 50);
        c.y = this.gridY.span(12) + Math.randint(-50, 50);

        return true;
      }
    }, this);
  },

  gameclear: function() {
    // add clear bonus
    var bonus = 2000;
    this.score += bonus;

    // add time bonus
    var seconds = (this.time/1000).floor();
    var bonusTime = Math.max(60*10-seconds, 0);
    this.score += (bonusTime*10);

    this.gameover();
  },

  gameover: function() {
    this.exit({
      score: this.score,
    });
  },

  _accessor: {
    score: {
      get: function() {
        return this._score;
      },
      set: function(v) {
        this._score = v;
        this.scoreLabel.text = v;
      },
    },
  },
});

/*
 * ブロック
 */
phina.define('Block', {
  superClass: 'RectangleShape',

  init: function(angle) {
    this.superInit({
      width: BLOCK_SIZE,
      height: BLOCK_SIZE,
      fill: 'hsl({0}, 80%, 60%)'.format(angle || 0),
      stroke: null,
      cornerRadius: 8,
    });
  },
});

/*
 * ボール
 */
phina.define('Ball', {
  superClass: 'CircleShape',

  init: function() {
    this.superInit({
      radius: BALL_RADIUS,
      fill: '#eee',
      stroke: null,
      cornerRadius: 8,
    });

    this.speed = 0;
    this.direction = Vector2(1, -1).normalize();
  },

  move: function() {
    this.x += this.direction.x;
    this.y += this.direction.y;
  },

  reflectX: function() {
    this.direction.x *= -1;
  },
  reflectY: function() {
    this.direction.y *= -1;
  },
});

/*
 * パドル
 */
phina.define('Paddle', {
  superClass: 'RectangleShape',
  init: function() {
    this.superInit({
      width: PADDLE_WIDTH,
      height: PADDLE_HEIGHT,
      fill: '#eee',
      stroke: null,
      cornerRadius: 8,
    });
  },

  hold: function(ball) {
    this.ball = ball;
  },

  release: function() {
    this.ball = null;
  },

  update: function() {
    if (this.ball) {
      this.ball.x = this.x;
      this.ball.y = this.top-this.ball.radius;
    }
  }
});


/*
 * パドル横
 */
phina.define('Paddle_h', {
  superClass: 'RectangleShape',
  init: function() {
    this.superInit({
      width: PADDLE_HEIGHT,
      height: PADDLE_WIDTH,
      fill: '#eee',
      stroke: null,
      cornerRadius: 8,
    });
  },

  hold: function(ball) {
    this.ball = ball;
  },

  release: function() {
    this.ball = null;
  },

  update: function() {
    if (this.ball) {
      this.ball.x = this.x;
      this.ball.y = this.top-this.ball.radius;
    }
  }
});


/*
 * コンボラベル
 */
phina.define('ComboLabel', {
  superClass: 'Label',
  init: function(num) {
    this.superInit(num + ' combo!');

    this.stroke = 'white';
    this.strokeWidth = 8;

    // 数によって色とサイズを分岐
    if (num < 5) {
      this.fill = 'hsl(40, 60%, 60%)';
      this.fontSize = 16;
    }
    else if (num < 10) {
      this.fill = 'hsl(120, 60%, 60%)';
      this.fontSize = 32;
    }
    else {
      this.fill = 'hsl(220, 60%, 60%)';
      this.fontSize = 48;
    }

    // フェードアウトして削除
    this.tweener
      .by({
        alpha: -1,
        y: -50,
      })
      .call(function() {
        this.remove();
      }, this)
      ;
  },
});


phina.main(function() {
  var app = GameApp({
    title: 'PONG',
    startLabel: location.search.substr(1).toObject().scene || 'title',
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    backgroundColor: '#444',
    autoPause: true,
    debug: false,
  });

  app.enableStats();

  app.run();
});

// メイン処理
//phina.main(function() {
  // アプリケーション生成
  //var app = GameApp({
    //startLabel: 'main', // メインシーンから開始する
  //});
  // アプリケーション実行
  //app.run();
//});
