# chat/urls.py
from django.urls import path

from . import views

urlpatterns = [
    path('game', views.routing_desk, name='routing_desk'),
    path('',views.mypage,name='mypage'),
    path('debug/chat/',views.index,name='chat_index'),
    path('debug/chat/<str:room_name>/', views.room, name='chat_room'),
    path('debug/reset/<str:num>/',views.resetDB, name='resetDB'),
    path('debug/PONG/index/',views.index_pong, name='pong_index'),
]
