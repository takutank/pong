from django.apps import AppConfig


class ChatConfig(AppConfig):
    name = 'chat'

class PONGConfig(AppConfig):
    name = 'pong'
