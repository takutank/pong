"""
from channels.generic.websocket import WebsocketConsumer
import json
class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
    def disconnect(self, close_code):
        pass
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        print("received message: " + message)
        self.send(text_data=json.dumps({
            'message': message
        }))

"""

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from .models import Rooms
import time

def create_message(event):
    message = {"type": event["type"], "message": event["message"]}
    return message

class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()
    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            { 'type': 'chat_message', 'message': message }
        )
    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        # Send message to WebSocket
        self.send(text_data=json.dumps({'message': message}))


class MatchingConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.username = self.scope['url_route']['kwargs']['username']
        self.room_group_name = 'matching_%s' % self.room_name
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

        # find player_number
        RoomDB = Rooms.objects.get(room_url=self.room_name)
        if RoomDB.Player0 == self.username:
            self.player_role = "Player0"
        elif RoomDB.Player1 == self.username:
            self.player_role = "Player1"
        elif RoomDB.Player2 == self.username:
            self.player_role = "Player2"
        elif RoomDB.Player3 == self.username:
            self.player_role = "Player3"

        # create message
        message = {
            "player_name": self.username,
            "player_num": self.player_role
        }
        # send connect_message to matching.html
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {'type': 'connect_message', 'message': message}
        )
        if RoomDB.player_num == 4:
            RoomDB.is_playing = True
            RoomDB.save()
            # send play_game message to matching.html
            message = {
                "DB":{
                    "player_num":RoomDB.player_num,
                    "Player0":RoomDB.Player0,
                    "Player1":RoomDB.Player1,
                    "Player2":RoomDB.Player2,
                    "Player3":RoomDB.Player3,
                    "room_url":RoomDB.room_url,
                    "is_playing":RoomDB.is_playing
                }
            }
            # 少し間隔を開ける
            time.sleep(0.5)
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {'type': 'play_game', 'message': message}
            )

    def disconnect(self, colse_code):
        RoomDB = Rooms.objects.get(room_url=self.room_name)
        if RoomDB.Player0 == self.username:
            RoomDB.Player0 = ""
        elif RoomDB.Player1 == self.username:
            RoomDB.Player1 = ""
        elif RoomDB.Player2 == self.username:
            RoomDB.Player2 = ""
        elif RoomDB.Player3 == self.username:
            RoomDB.Player3 = ""
        RoomDB.player_num -= 1
        RoomDB.save()
        # Leave room group
        message = {
            "disconnected_player":self.player_role
        }
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            { "type":"disconnect_message", "message":message}
        )
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        # receive json_data from websocket(channel)
        text_data_json = json.loads(text_data)
        type_json = text_data_json["type"]
        message = text_data_json["message"]
        # 
        if type_json == "sync_user":
            pass
        elif type_json == "status_update":
            if message["status"] == "gameover":
                #ここにis_playingをfalseにするコードを書く
                pass
        else:
            return 0
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            { 'type': type_json, 'message': message }
        )    
    def sync_user(self,event):
        self.send(text_data=json.dumps({'message':create_message(event)}))
    
    def status_update(self,event):
        self.send(text_data=json.dumps({'message':create_message(event)}))

    # forMatching
    def connect_message(self,event):
        self.send(text_data=json.dumps({'message':create_message(event)}))
    
    def play_game(self,event):
        self.send(text_data=json.dumps({'message':create_message(event)}))

    def disconnect_message(self,event):
        self.send(text_data=json.dumps({'message':create_message(event)}))
