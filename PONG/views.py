from django.shortcuts import render
from django.utils.safestring import mark_safe
import json
from .models import Rooms
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

# <----- views for chat ----->
# Create your views here.
def index(request):
    return render(request, 'chat/index.html', {})

def index_pong(request):
    return render(request, 'PONG/index.html', {})

def room(request, room_name):
    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    })

# <----- ----->

@login_required
def routing_desk(request):
    """アクセスしてきたユーザーをどのRoomにリダイレクトするかを決定する関数。
    
    Arguments:
        request {none} -- http request
    
    Returns:
        None -- roomidを決定したあと、roomidでフィルタしたModelのデータをMatching.htmlへ渡す
    """
    username = str(request.user)
    #username = "testuser"
    # 空き部屋があるか調べる
    RoomDB = Rooms.objects.all().filter(player_num__lt=4, is_playing = False).order_by('pk')
    print(RoomDB)
    if RoomDB != None:
        # global変数
        Roomid = 5
        player_role=""
        # 空き部屋があればRoomDBに名前を登録 (脳停止コード)
        for room in RoomDB:
            if room.Player0 == "":
                room.Player0 = username
                Roomid = room.id
                player_role = "Player0"
                room.save()
                break
            elif room.Player1 == "":
                room.Player1 = username
                Roomid = room.id
                player_role = "Player1"
                room.save()
                break
            elif room.Player2 == "":
                room.Player2 = username
                Roomid = room.id
                player_role = "Player2"
                room.save()
                break
            elif room.Player3 == "":
                room.Player3 = username
                Roomid = room.id
                player_role = "Player3"
                room.save()
                break
            else:
                continue
        
        # メタデータ更新
        if Roomid==5:
            return render(request,"PONG/mypage.html",{})
        RoomDB = Rooms.objects.get(id=Roomid)
        num = RoomDB.player_num
        RoomDB.player_num = num+1
        RoomDB.save()

        # matching.htmlにリダイレクトするためのデータの用意
        data = {
            "RoomDB":RoomDB,
            "username":username,
            "player_role":player_role
        }

        return render(request,"PONG/matching.html",data)

    else:
        # 空き部屋が無ければ満員の旨を表示して（戻る？）
        return render(request,"PONG/mypage.html",{})

def mypage(request):
    return render(request,"PONG/mypage.html",{})


def resetDB(request,num):
    """データベースをリセットしたいときに実行するやつ（デバッグ用）
    debug/reset/all -- 全てのDBをリセット
    debug/reset/([1-4]) -- numberへ渡されたidのレコードをリセット

    Arguments:
        request {[type]} -- http request
        num {[type]} -- 入力された時はidで検索する
    """
    if num == "all":
        RoomDB = Rooms.objects.all().order_by('pk')
    else:
        RoomDB = Rooms.objects.all().filter(id=num)
    for room in RoomDB:
        room.Player0 = ""
        room.Player1 = ""
        room.Player2 = ""
        room.Player3 = ""
        room.player_num = 0
        room.is_playing = False
        room.save()
    return render(request,"PONG/mypage.html",{})
