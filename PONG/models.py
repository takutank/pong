from django.db import models

# Create your models here.
class Rooms(models.Model):
    player_num = models.IntegerField(default=0)
    Player0 = models.TextField(max_length=20,default="")
    Player1 = models.TextField(max_length=20,default="")
    Player2 = models.TextField(max_length=20,default="")
    Player3 = models.TextField(max_length=20,default="")
    room_url = models.TextField(default="")
    is_playing = models.BooleanField(default=False)
